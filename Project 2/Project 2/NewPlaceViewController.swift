//
//  NewPlaceViewController.swift
//  Project 2
//
//  Created by Zach Wegrzyniak on 10/21/14.
//  Copyright (c) 2014 Zach Wegrzyniak. All rights reserved.
//

import Foundation
import CoreData
import MapKit
import UIKit

class NewPlace : UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var addedPicture: UIImageView!
    var latitude: Double!
    var longitude: Double!
    
    lazy var managedObjectContext : NSManagedObjectContext? = {
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        if let managedObjectContext = appDelegate.managedObjectContext {
            return managedObjectContext
        }
        else {
            return nil
        }
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func addLocation(sender: AnyObject) {
        if name != nil && descriptionTextField != nil {
            Place.createInManagedObjectContext(managedObjectContext!, title: name!.text, subtitle: descriptionTextField.text, latitude: latitude, longitude: longitude)
        }
    }
    
    @IBAction func addPicture(sender: AnyObject) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        if let chosenImage: UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let imageView = UIImageView(image: chosenImage)
            self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 100)
            self.view.addSubview(imageView)
            self.view.sendSubviewToBack(imageView)
            picker.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}