//
//  AnnotationInfo.swift
//  Project 2
//
//  Created by Zach Wegrzyniak on 10/19/14.
//  Copyright (c) 2014 Zach Wegrzyniak. All rights reserved.
//

import Foundation
import CoreData
import MapKit

class Place: NSManagedObject, MKAnnotation, Equatable, Printable {
    @NSManaged var name: NSString
    @NSManaged var descriptor: NSString
    @NSManaged var latitude: Double
    @NSManaged var longitude: Double
    
    var coordinate: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        }
    }
    
    var title: String! {
        get {
            return name
        }
    }
    
    var subtitle: String! {
        get {
            return descriptor
        }
    }
    
    override var description: String {
        get {
            return "Place: \(name) \(latitude) \(longitude)"
        }
    }
    
    class func createInManagedObjectContext(moc: NSManagedObjectContext, title: NSString, subtitle: NSString, latitude: Double, longitude: Double) -> Place {
        let newItem = NSEntityDescription.insertNewObjectForEntityForName("Place", inManagedObjectContext: moc) as Place
        newItem.name = title
        newItem.descriptor = subtitle
        newItem.latitude = latitude
        newItem.longitude = longitude
        
        return newItem
    }
}

func ==(lhs: Place, rhs: Place) -> Bool {
    return lhs.title == rhs.title
}