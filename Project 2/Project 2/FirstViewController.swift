//
//  FirstViewController.swift
//  Project 2
//
//  Created by Zach Wegrzyniak on 10/19/14.
//  Copyright (c) 2014 Zach Wegrzyniak. All rights reserved.
//

import UIKit
import MapKit
import CoreData
import Alamofire
import CoreLocation

let METERS_PER_MILE = 1609.344

class FirstViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    lazy var managedObjectContext : NSManagedObjectContext? = {
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        if let managedObjectContext = appDelegate.managedObjectContext {
            return managedObjectContext
        }
        else {
            return nil
        }
        }()
    
    lazy var locationManager : CLLocationManager? = {
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        if let locationManager = appDelegate.locationManager {
            return locationManager
        }
        else {
            return nil
        }
        }()
    
    var currentPoints: [Place] {
        get {
            return getCurrentPoints()
        }
    }
    @IBOutlet weak var mapView: MKMapView!
    
    @IBAction func refreshPoints(sender: AnyObject) {
        pullFromServer()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        pullFromServer()
        locationManager!.delegate = self
        let coordinates = mapView.userLocation.coordinate
        println("\(coordinates.latitude) \(coordinates.longitude)")
        let defaultCenter = NSNotificationCenter.defaultCenter()
        defaultCenter.addObserver(self, selector: "receiveLocation:", name: "addLocation", object: self)
    }
    
    override func viewWillAppear(animated: Bool) {
        var zoomLocation = CLLocationCoordinate2D(latitude: 43.6, longitude: -116.2)
        var viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 1.5 * METERS_PER_MILE, 1.5 * METERS_PER_MILE)
        mapView.setRegion(viewRegion, animated: true)
        view.sendSubviewToBack(mapView)

        mapView.showsUserLocation = true
    }
    
    func pullFromServer() {
        Alamofire.request(.GET, "http://zstudiolabs.com/labs/compsci402/buildings.json")
            .responseJSON { (_, _, unowned JSON, _) in
                if let response = JSON as? Array<NSDictionary> {
                    var newAnnotations: AnnotationBuilder = AnnotationBuilder()
                    newAnnotations.addAll(response, existing: self.currentPoints)
                    self.pinPoints()
                }
        }
    }
    
    func pinPoints() {
        var results = map(self.currentPoints, {self.mapView.addAnnotation($0)})
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if let vc: NewPlace = segue.destinationViewController as? NewPlace {
            let coordinate = mapView.userLocation.coordinate
            vc.latitude = coordinate.latitude
            vc.longitude = coordinate.longitude
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

