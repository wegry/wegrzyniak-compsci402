//
//  AnnotationInfoList.swift
//  Project 2
//
//  Created by Zach Wegrzyniak on 10/19/14.
//  Copyright (c) 2014 Zach Wegrzyniak. All rights reserved.
//

import Foundation
import CoreData
import MapKit

class AnnotationBuilder: NSObject {
    
    lazy var managedObjectContext : NSManagedObjectContext? = {
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        if let managedObjectContext = appDelegate.managedObjectContext {
            return managedObjectContext
        }
        else {
            return nil
        }
        }()
    
    func add(blob: NSDictionary, existing: [Place]) -> Place? {
        let name = blob["name"] as? String
        let description = blob["description"] as? String
        
        let coordinate = blob["location"] as? NSDictionary
        let latitude = coordinate!["latitude"] as? Double
        let longitude = coordinate!["longitude"] as? Double
        
        
        if name != nil && description != nil && coordinate != nil {
            var oldMatches = existing.filter{$0.latitude == latitude! && $0.longitude == longitude!}
            if oldMatches.count != 0 {
                return nil
            }
            var annotation = Place.createInManagedObjectContext(self.managedObjectContext!, title: name!, subtitle: description!, latitude: latitude!, longitude: longitude!)
            return annotation
        }
        
        return nil
    }
    
    func addAll(response: Array<NSDictionary>, existing: [Place]) -> [Place] {
        let raw = map(response, {self.add($0, existing: existing)})
        var cleaned = [Place]()
        for item in raw {
            if let unwrappedPlace = item {
                cleaned.append(unwrappedPlace)
            }
        }
        return cleaned
    }
}

func getCurrentPoints() -> [Place] {
    let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    if let managedObjectContext = appDelegate.managedObjectContext {
        
        var request = NSFetchRequest(entityName: "Place")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        return managedObjectContext.executeFetchRequest(request, error: nil) as [Place]
        
    }
    return [Place]()
}