//
//  MapResultViewController.swift
//  Final Project
//
//  Created by Zach Wegrzyniak on 11/26/14.
//  Copyright (c) 2014 Zach Wegrzyniak. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class MapResultViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UITabBarControllerDelegate {
    
    let METERS_PER_MILE = 1609.344
    
    
    let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    var route: Route {
        get {
            return appDelegate.route!
        }
    }

    lazy var locationManager : CLLocationManager? = {
        if let locationManager = self.appDelegate.locationManager {
            return locationManager
        }
        else {
            return nil
        }
        }()
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.showAnnotations(route.optimalRoute, animated: true)
        var startCopy = route.start
        startCopy.title = "Start"
        mapView.selectAnnotation(startCopy, animated: true)
        let routePath = MKGeodesicPolyline(coordinates: UnsafeMutablePointer(route.optimalRouteAsCoordinates), count: route.optimalRoute.count)
        mapView.addOverlay(routePath)
    }
    
    override func viewWillAppear(animated: Bool) {
        mapView!.delegate = self
        locationManager!.delegate = self
        locationManager!.requestWhenInUseAuthorization()
        mapView.showsUserLocation = true
    }

    //Anna -- StackOverflow
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKPolyline {
            var polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.greenColor()
            polylineRenderer.lineWidth = 1
            return polylineRenderer
        }
        return nil
    }
}