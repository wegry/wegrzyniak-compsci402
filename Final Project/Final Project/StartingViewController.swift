//
//  ViewController.swift
//  Final Project
//
//  Created by Zach Wegrzyniak on 11/3/14.
//  Copyright (c) 2014 Zach Wegrzyniak. All rights reserved.
//

import UIKit
import MapKit

class StartingViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {
    let METERS_PER_MILE = 1609.344
    lazy var locationManager : CLLocationManager? = {
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        if let locationManager = appDelegate.locationManager {
            return locationManager
        }
        else {
            return nil
        }
        }()
    
    var startingLocation: ValidatingAnnotation?
    var changesToStartString = 0
    
    @IBOutlet weak var startTextField: UITextField!
    @IBOutlet weak var addressCheckLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager!.delegate = self
        startTextField!.delegate = self
        startTextField.placeholder = "Where are you starting?"
        
        //Testing
        startTextField.text = "1601 W Bannock St, Boise, ID"
        var zoomLocation = CLLocationCoordinate2D(latitude: 43.619, longitude: -116.2)
        let viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5 * METERS_PER_MILE, 2.0 * METERS_PER_MILE)
        mapView.setRegion(viewRegion, animated: true)
        checkTextFieldForAddress(startTextField)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        objc_sync_enter(self)
        changesToStartString += 1
        if changesToStartString > 1 {
            startingLocation = nil
            mapView.removeAnnotations(mapView.annotations)
        }
        objc_sync_exit(self)
        checkTextFieldForAddress(startTextField)
        return true
    }
    
    func checkTextFieldForAddress(textField: UITextField) {
        if !textField.text.isEmpty {
            startingLocation = ValidatingAnnotation(address: textField.text, postValidationCallback)
            startingLocation!.validate(locationManager?.location?.coordinate)
        }
        else {
            addressCheckLabel.text = "?"
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        if let location = startingLocation {
            if location.checkedAndValidated {
                return true
            }
        }
        else if startTextField.text.isEmpty {
            var refreshAlert = UIAlertController(title: "What's that?", message: "We need a location to get started?", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Alright...", style: .Default, handler: { (action: UIAlertAction!) in
            }))
            
            presentViewController(refreshAlert, animated: true, completion: nil)
        }
        else {
            var refreshAlert = UIAlertController(title: "We can't find that location", message: "Maybe there's been a mistake?", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            }))
            
            presentViewController(refreshAlert, animated: true, completion: nil)
        }
        return false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let nextView = segue.destinationViewController as? IntermediateViewController {
            nextView.startLocation = self.startingLocation!
        }
    }
    
    func postValidationCallback(outcome: Bool, point: MKPlacemark?) {
        if outcome {
            if let pointLocation = point {
                addressCheckLabel!.text = "✅"
                var newPoint = ValidatingAnnotation(address: startTextField.text)
                newPoint.point = point
                mapView.showAnnotations([newPoint], animated: true)
            }
            else {
                addressCheckLabel!.text = "?"
            }
        }
        else {
            addressCheckLabel!.text = "❌"
            mapView.removeAnnotations(mapView.annotations)
        }
        addressCheckLabel!.hidden = false
        addressCheckLabel.reloadInputViews()
    }
}
