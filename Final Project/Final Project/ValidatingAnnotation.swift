//
//  IntermediateRowItem.swift
//  Final Project
//
//  Created by Zach Wegrzyniak on 11/30/14.
//  Copyright (c) 2014 Zach Wegrzyniak. All rights reserved.
//

import Foundation
import MapKit

class ValidatingAnnotation: NSObject, Printable, MKAnnotation {
    var validated = false
    var startedChecking = false
    var title: String
    var point: MKPlacemark?
    var sideEffect: ((Bool, MKPlacemark?) -> ())?
    
    var coordinate: CLLocationCoordinate2D {
        get {
            return point!.coordinate
        }
    }
    
    var checkedAndValidated: Bool {
        get {
            return validated && startedChecking
        }
    }
    
    override var description: String {
        get {
            return title
        }
    }
    
    init (address: String) {
        title = address
    }
    
    convenience init (address: String, effect: ((Bool, MKPlacemark?) -> ())) {
        self.init(address: address)
        self.sideEffect = effect
    }
    
    func validate(start: CLLocationCoordinate2D?) {
        if checkedAndValidated {
            return
        }
        
        let searchRequest = MKLocalSearchRequest()
        if let startLocation = start {
            let region = MKCoordinateRegionMakeWithDistance(startLocation, 15000, 15000)
            searchRequest.region = region
        }
        searchRequest.naturalLanguageQuery = title
        let localSearch = MKLocalSearch(request: searchRequest)
        localSearch.startWithCompletionHandler(processAddress)
    }
    
    func processAddress(results: MKLocalSearchResponse!, errors: NSError!) {
        startedChecking = true

        if errors != nil {
            if let effect = sideEffect {
                effect(false, nil)
            }
            return
        }
        
        println("There were \(results.mapItems.count) address result(s) for \(title).")
        for item in results.mapItems {
            if let mapItem = item as? MKMapItem {
                if let placemark = mapItem.placemark {
                    validated = true
                    point = placemark
                    if let effect = sideEffect {
                        effect(true, placemark)
                    }
                    break
                }
            }
        }
    }
}