//
//  Route.swift
//  Final Project
//
//  Created by Zach Wegrzyniak on 11/29/14.
//  Copyright (c) 2014 Zach Wegrzyniak. All rights reserved.
//

import Foundation
import MapKit

class Route {
    var start: ValidatingAnnotation
    var interiorPoints: [ValidatingAnnotation]
    var optimal: [ValidatingAnnotation]?
    
    var optimalRoute: [ValidatingAnnotation] {
        get {
            return optimal!
        }
    }
    
    var allAnnotations: [ValidatingAnnotation] {
        get {
            return [start] + interiorPoints
        }
    }
    
    var optimalRouteAsCoordinates: [CLLocationCoordinate2D] {
        get {
            return map(optimalRoute, {$0.point!.coordinate})
        }
    }
    
    var interiorPointsAsCLLocations: [CLLocation] {
        get {
            return map(interiorPoints, {CLLocation(latitude: $0.point!.coordinate.latitude, longitude: $0.point!.coordinate.longitude)})
        }
    }
    
    init (start: ValidatingAnnotation, points: [ValidatingAnnotation]) {
        self.start = start
        interiorPoints = points
        optimal = nearestNeighborPathfinding()
    }
    
    func nearestNeighborPathfinding() -> [ValidatingAnnotation] {
        let result = reduce(blendArrayWithStartAndEnd(interiorPoints, start), allAnnotations, {self.routeLength($0) < self.routeLength($1) ? $0 : $1})
        return result
    }
    
    func routeLength(points: [ValidatingAnnotation]) -> Double {
        var current = points[0]
        var totalDistance = 0.0
        for (i, point) in enumerate(points) {
            if i + 1 < points.count {
                totalDistance += point.point!.location.distanceFromLocation(points[i + 1].point!.location)
            }
        }
        return totalDistance
    }
    
//    var asDistanceMatrix: [[Double]] {
//        get {
//            var distanceMatrix: [[Double]] = []
//            let points = interiorPointsAsCLLocations
//            for (i, firstPoint) in enumerate(interiorPoints) {
//                var row: [Double] = []
//                let firstPlacemark = points[i]
//                for (j, secondPoint) in enumerate(interiorPoints) {
//                    var newItem = 0.0
//                    if i != j {
//                        let secondPlacemark = points[j]
//                        newItem = firstPlacemark.distanceFromLocation(secondPlacemark)
//                    }
//                    row.append(newItem)
//                }
//                distanceMatrix.append(row)
//            }
//            return distanceMatrix
//        }
//    }
}

func blendArrayWithStartAndEnd<T>(var array: Array<T>, start: T) -> Array<Array<T>>{
    return combos(array, array.count).map({[start] + $0 + [start]})
}

//http://stackoverflow.com/questions/25162500/apple-swift-generate-combinations-with-repetition
//Heavily modified from Martin R's answer
func sliceArray<T>(var arr: Array<T>, x1: Int, x2: Int) -> Array<T> {
    var tt: Array<T> = []
    for var ii = x1; ii <= x2; ++ii {
        tt.append(arr[ii])
    }
    return tt
}

func combos<T>(var array: Array<T>, k: Int) -> [[T]] {
    var subI : Int
    
    var ret = [[T]]()
    var sub = [[T]]()
    var next = [T]()
    for (i, current) in enumerate(array) {
        if(k == 1){
            ret.append([current])
        }
        else {
            sub = combos(sliceArray(array, i + 1, array.count - 1), k - 1)
            for subElement in sub {
                next = subElement
                next.insert(current, atIndex: 0)
                ret.append(next)
            }
        }
        
    }
    return ret
}