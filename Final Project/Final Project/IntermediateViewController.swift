import UIKit
import MapKit

class IntermediateViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    /*WE <3 SWIFT*/
    @IBOutlet weak var placeAddingField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var startLocation: ValidatingAnnotation?
    
    var items = [
        "Big City Coffee, 83702",
        "Dawsons 83702",
        "Java Downtown, 83702",
        "Zoo Boise",
        "Winco, 83702",
        "Whole Foods, Boise",
        "Boise Co-op",
        "Albertsons 83702"
        ].map {ValidatingAnnotation(address: $0)}
    
    @IBAction func addPlace(sender: AnyObject) {
        if placeAddingField.text.isEmpty { return }
        items = items + [ValidatingAnnotation(address: placeAddingField.text)]
        tableView.reloadData()
        placeAddingField.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.frame = self.view.frame
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
    }
    
    override func viewWillAppear(animated: Bool) {
        placeAddingField.placeholder = "Where else will you go?"
    }
    
    // MARK: TableView
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as? UITableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "cell")
        }
       
        let row = items[indexPath.row]
        
        cell!.textLabel!.text = "\(row.title)"
        validate(row)
        
        if row.validated {
            cell!.backgroundColor = UIColor.greenColor()
        }
        else if row.startedChecking {
            cell!.backgroundColor = UIColor.yellowColor()
        }
        else {
            cell!.backgroundColor = UIColor.whiteColor()
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        println("You selected cell #\(indexPath.row)!")
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        items.removeAtIndex(indexPath.row)
        tableView.reloadData()
    }
    
    
    //Segue stuff
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        let validatedItems = items.filter({$0.validated})
        let validated = validatedItems.count
        tableView.reloadData()
        if validated > 0 && items.count == validated {
            let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
            appDelegate.route = Route(start: startLocation!, points: validatedItems)
            return true
        }
        else if validated < items.count {
            var refreshAlert = UIAlertController(title: "Oh no!", message: "Waiting for \(items.count - validated) item(s) to be validated.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            }))
            
            presentViewController(refreshAlert, animated: true, completion: nil)
            tableView.reloadData()
        }
        else {
            var refreshAlert = UIAlertController(title: "Woah tiger", message: "We need some valid addresses to go out to. Enter at least one.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok...", style: .Default, handler: { (action: UIAlertAction!) in
            }))
            
            presentViewController(refreshAlert, animated: true, completion: nil)
        }
        return false
    }
    
    func validate(point: ValidatingAnnotation) {
        if !point.checkedAndValidated {
            point.validate(self.startLocation!.coordinate)
        }
    }
}