//
//  main.m
//  Project 1
//
//  Created by Zach Wegrzyniak on 9/11/14.
//  Copyright (c) 2014 Mobile dev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "P1AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([P1AppDelegate class]));
    }
}
