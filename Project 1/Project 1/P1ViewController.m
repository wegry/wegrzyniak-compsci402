//
//  P1ViewController.m
//  Project 1
//
//  Created by Zach Wegrzyniak on 9/11/14.
//  Copyright (c) 2014 Mobile dev. All rights reserved.
//

#import "P1ViewController.h"
#import "P1AnnotationInfo.h"
#import "AFNetworking.h"
#import "AddPlace.h"

static NSMutableArray *annotations = nil;

@interface P1ViewController ()

@end

#define METERS_PER_MILE 1609.344

@implementation P1ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!annotations) {
        annotations = [[NSMutableArray alloc] init];
        NSURL *URL = [NSURL URLWithString:@"http://zstudiolabs.com/labs/compsci402/buildings.json"];
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                             initWithRequest:request];
        
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"%@", responseObject);
            NSArray *listOfDictionaries = responseObject;
            for (NSDictionary *blob in listOfDictionaries) {
                P1AnnotationInfo *annotation = [P1AnnotationInfo initFromBlob:blob];
                if (annotation) {
                    [annotations addObject:annotation];
                    [_mapView addAnnotation:annotation];
                }
            }
        } failure:nil];
        [operation start];
    }
    else {
        for (P1AnnotationInfo *annotation in annotations) {
            [_mapView addAnnotation:annotation];
        }
    }
    [_mapView setShowsUserLocation: YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNewPoint:) name:@"addPoint" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    // 1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 43.6;
    zoomLocation.longitude= -116.2;
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 1.5*METERS_PER_MILE, 1.5*METERS_PER_MILE);
    // 3
    [_mapView setRegion:viewRegion animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        [_clockBackground setHidden:YES];
    } else {
        [_clockBackground setHidden:NO];
    }
}

- (void) receiveNewPoint: (NSNotification*) notification {
    if ([[notification name] isEqualToString:@"addPoint"]) {
        AddPlace *place = notification.object;
        NSString *name = place.nameTextField.text;
        NSString *description = place.descriptionTextField.text;
        CLLocationCoordinate2D coordisnate = _mapView.userLocation.coordinate;
        UIImage *image = place.placeImage;
        
        if (!place.placeImage) {
            P1AnnotationInfo *annotation = [[P1AnnotationInfo alloc] initWithName: name description:description coordinate:coordinate];
            [annotations addObject:annotation];
        }
        else {
            P1AnnotationInfo *annotation = [[P1AnnotationInfo alloc] initWithImage:name description:description coordinate:coordinate image:image];
            [_mapView addAnnotation:annotation];
            [annotations addObject:annotation];
        }
    }
}
@end
