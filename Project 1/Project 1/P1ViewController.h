//
//  P1ViewController.h
//  Project 1
//
//  Created by Zach Wegrzyniak on 9/11/14.
//  Copyright (c) 2014 Mobile dev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface P1ViewController : UIViewController
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *clockBackground;
@property CLLocationManager * locationManager;


@end

