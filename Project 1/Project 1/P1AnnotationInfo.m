//
//  P1AnnotationInfo.m
//  Project 1
//
//  Created by Zach Wegrzyniak on 9/11/14.
//  Copyright (c) 2014 Mobile dev. All rights reserved.
//

#import "P1AnnotationInfo.h"

@interface P1AnnotationInfo ()
@property (nonatomic, copy) NSString *name;
@property (nonatomic) NSString *shortDescription;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) UIImage *image;
@end

@implementation P1AnnotationInfo

- (id)initWithName:(NSString*)name description:(NSString *)description  coordinate:(CLLocationCoordinate2D)coordinate {
    if ((self = [super init])) {
        if ([name isKindOfClass:[NSString class]]) {
            self.name = name;
        } else {
            self.name = @"Why oh, why oh did I ever leave Ohio?";
        }
        self.shortDescription = description;
        self.coordinate = coordinate;
    }
    return self;
}

- (id) initWithImage:(NSString *)name description:(NSString *)description coordinate:(CLLocationCoordinate2D)coordinate image:(UIImage *)image {
    self = [self initWithName:name description:description coordinate:coordinate];
    self.image = image;
    return self;
}

+ (P1AnnotationInfo *) initFromBlob:(NSDictionary *)blob {
    NSNumber *latitude = blob[@"location"][@"latitude"];
    NSNumber *longitude = blob[@"location"][@"longitude"];
    NSString *name = blob[@"name"];
    NSString *description = blob[@"description"];
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = latitude.doubleValue;
    coordinate.longitude = longitude.doubleValue;
    P1AnnotationInfo *annotation = [[P1AnnotationInfo alloc] initWithName: name description:description coordinate:coordinate];
    return annotation;
}

- (NSString *)title {
    return _name;
}

- (NSString *)subtitle {
    return _shortDescription;
}

- (CLLocationCoordinate2D)coordinate {
    return _coordinate;
}

- (UIImage *)image {
    return _image;
}

@end