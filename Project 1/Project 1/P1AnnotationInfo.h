//
//  P1AnnotationInfo.h
//  Project 1
//
//  Created by Zach Wegrzyniak on 9/11/14.
//  Copyright (c) 2014 Mobile dev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>


@interface P1AnnotationInfo : NSObject <MKAnnotation>

- (id)initWithName:(NSString*)name description:(NSString*)description coordinate:(CLLocationCoordinate2D)coordinate;
- (id)initWithImage:(NSString*)name description:(NSString*)description coordinate:(CLLocationCoordinate2D)coordinate image:(UIImage *)image;
+ (id)initFromBlob:(NSDictionary *)blob;

@end
