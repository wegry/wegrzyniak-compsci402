//
//  P1PointFactory.h
//  
//
//  Created by Zach Wegrzyniak on 9/20/14.
//
//

#import <Foundation/Foundation.h>
#import "P1AnnotationInfo.h"
#import <MapKit/MapKit.h>

@interface P1PointFactory : NSObject

+(void) addPoint: (NSString*)description coordinate:(CLLocationCoordinate2D) coordinate;

@end
