//
//  P1AppDelegate.h
//  Project 1
//
//  Created by Zach Wegrzyniak on 9/11/14.
//  Copyright (c) 2014 Mobile dev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface P1AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
