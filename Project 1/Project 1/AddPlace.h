//
//  AddPlace.h
//  Project 1
//
//  Created by Zach Wegrzyniak on 9/20/14.
//  Copyright (c) 2014 Mobile dev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface AddPlace : UIViewController<UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *descriptionTextField;
@property (weak, nonatomic) UIImage *placeImage;
@property (weak, nonatomic) IBOutlet UIImageView *addedPicture;

- (IBAction)addPlace:(id)sender;
- (IBAction)addPicture:(id)sender;
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;

@end

