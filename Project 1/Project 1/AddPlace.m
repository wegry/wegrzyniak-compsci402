//
//  AddPlace.m
//  Project 1
//
//  Created by Zach Wegrzyniak on 9/20/14.
//  Copyright (c) 2014 Mobile dev. All rights reserved.
//

#import "AddPlace.h"

@interface AddPlace ()
@property (weak, nonatomic) IBOutlet UIButton *addPlaceButton;
@end

@implementation AddPlace

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)addPlace:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"addPoint" object:self];
}

- (IBAction)addPicture:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];

}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    //_placeImage = chosenImage;
//_addedPicture.image = chosenImage;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:chosenImage];
    self.view.backgroundColor = [[UIColor alloc] initWithRed:0 green:0 blue:0 alpha:100];
    [self.view addSubview:imageView];
    [self.view sendSubviewToBack:imageView];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
@end
